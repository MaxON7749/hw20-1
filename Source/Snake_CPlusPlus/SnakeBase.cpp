// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeBase.h"
#include "SnakeElementBase.h"


// Sets default values
ASnakeBase::ASnakeBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	ElementSize = 100.f;
	MovementSpeed = 10.f;
	LastMoveDirection = EMovementDirection::UP;

}

// Called when the game starts or when spawned
void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();
	SetActorTickInterval(MovementSpeed);
	//GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, GetActorTransform());
	AddSnakeElement(4);
	
}

// Called every frame
void ASnakeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	Move();
	

}

void ASnakeBase::AddSnakeElement(int ElemetnsNum)
{
	for (int i = 0; i < ElemetnsNum; ++i) 
	{
		FVector NewLocation(SnakeElments.Num() * ElementSize, 0, 0);
		FTransform NewTransform(NewLocation);
		ASnakeElementBase* NewSnakeElem = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);
		//NewSnakeElem->AttachToActor(this, FAttachmentTransformRules::KeepRelativeTransform);
		int32 ElemIndx= SnakeElments.Add(NewSnakeElem);
		if (ElemIndx == 0) 
		{
			NewSnakeElem->SetFirstElementType();
		}
		
	}
}

void ASnakeBase::Move()
{
	FVector MovementVector(ForceInitToZero);
	float MovementSpeedMv=ElementSize;

	switch (LastMoveDirection) 
	{
	case EMovementDirection::UP:
		MovementVector.X += MovementSpeedMv;
		break;
	case EMovementDirection::DOWN:
		MovementVector.X -= MovementSpeedMv;
		break;
	case EMovementDirection::LEFT:
		MovementVector.Y += MovementSpeedMv;
		break;
	case EMovementDirection::RIGHT:
		MovementVector.Y -= MovementSpeedMv;
		break;
	}

	//AddActorWorldOffset(MovementVector);

	for (int i = SnakeElments.Num() - 1; i > 0; i--) 
	{
		auto CurrentElement = SnakeElments[i];
		auto PrevElement = SnakeElments[i - 1];
		FVector PrevLocation = PrevElement->GetActorLocation();
		CurrentElement->SetActorLocation(PrevLocation);
	}

	SnakeElments[0]->AddActorWorldOffset(MovementVector);
}

