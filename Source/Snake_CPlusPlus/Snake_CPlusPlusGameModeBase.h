// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Snake_CPlusPlusGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class SNAKE_CPLUSPLUS_API ASnake_CPlusPlusGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
