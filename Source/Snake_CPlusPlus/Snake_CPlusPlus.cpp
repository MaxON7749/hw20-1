// Copyright Epic Games, Inc. All Rights Reserved.

#include "Snake_CPlusPlus.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, Snake_CPlusPlus, "Snake_CPlusPlus" );
