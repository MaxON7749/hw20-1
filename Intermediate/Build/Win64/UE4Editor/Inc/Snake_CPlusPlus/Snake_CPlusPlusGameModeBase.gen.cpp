// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Snake_CPlusPlus/Snake_CPlusPlusGameModeBase.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeSnake_CPlusPlusGameModeBase() {}
// Cross Module References
	SNAKE_CPLUSPLUS_API UClass* Z_Construct_UClass_ASnake_CPlusPlusGameModeBase_NoRegister();
	SNAKE_CPLUSPLUS_API UClass* Z_Construct_UClass_ASnake_CPlusPlusGameModeBase();
	ENGINE_API UClass* Z_Construct_UClass_AGameModeBase();
	UPackage* Z_Construct_UPackage__Script_Snake_CPlusPlus();
// End Cross Module References
	void ASnake_CPlusPlusGameModeBase::StaticRegisterNativesASnake_CPlusPlusGameModeBase()
	{
	}
	UClass* Z_Construct_UClass_ASnake_CPlusPlusGameModeBase_NoRegister()
	{
		return ASnake_CPlusPlusGameModeBase::StaticClass();
	}
	struct Z_Construct_UClass_ASnake_CPlusPlusGameModeBase_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ASnake_CPlusPlusGameModeBase_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AGameModeBase,
		(UObject* (*)())Z_Construct_UPackage__Script_Snake_CPlusPlus,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ASnake_CPlusPlusGameModeBase_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * \n */" },
		{ "HideCategories", "Info Rendering MovementReplication Replication Actor Input Movement Collision Rendering Utilities|Transformation" },
		{ "IncludePath", "Snake_CPlusPlusGameModeBase.h" },
		{ "ModuleRelativePath", "Snake_CPlusPlusGameModeBase.h" },
		{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_ASnake_CPlusPlusGameModeBase_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ASnake_CPlusPlusGameModeBase>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ASnake_CPlusPlusGameModeBase_Statics::ClassParams = {
		&ASnake_CPlusPlusGameModeBase::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x009002ACu,
		METADATA_PARAMS(Z_Construct_UClass_ASnake_CPlusPlusGameModeBase_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ASnake_CPlusPlusGameModeBase_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ASnake_CPlusPlusGameModeBase()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ASnake_CPlusPlusGameModeBase_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ASnake_CPlusPlusGameModeBase, 1059811843);
	template<> SNAKE_CPLUSPLUS_API UClass* StaticClass<ASnake_CPlusPlusGameModeBase>()
	{
		return ASnake_CPlusPlusGameModeBase::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ASnake_CPlusPlusGameModeBase(Z_Construct_UClass_ASnake_CPlusPlusGameModeBase, &ASnake_CPlusPlusGameModeBase::StaticClass, TEXT("/Script/Snake_CPlusPlus"), TEXT("ASnake_CPlusPlusGameModeBase"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ASnake_CPlusPlusGameModeBase);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
