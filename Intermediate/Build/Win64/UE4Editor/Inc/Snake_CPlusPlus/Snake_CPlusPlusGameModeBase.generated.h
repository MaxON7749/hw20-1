// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SNAKE_CPLUSPLUS_Snake_CPlusPlusGameModeBase_generated_h
#error "Snake_CPlusPlusGameModeBase.generated.h already included, missing '#pragma once' in Snake_CPlusPlusGameModeBase.h"
#endif
#define SNAKE_CPLUSPLUS_Snake_CPlusPlusGameModeBase_generated_h

#define Snake_CPlusPlus_Source_Snake_CPlusPlus_Snake_CPlusPlusGameModeBase_h_15_SPARSE_DATA
#define Snake_CPlusPlus_Source_Snake_CPlusPlus_Snake_CPlusPlusGameModeBase_h_15_RPC_WRAPPERS
#define Snake_CPlusPlus_Source_Snake_CPlusPlus_Snake_CPlusPlusGameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define Snake_CPlusPlus_Source_Snake_CPlusPlus_Snake_CPlusPlusGameModeBase_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesASnake_CPlusPlusGameModeBase(); \
	friend struct Z_Construct_UClass_ASnake_CPlusPlusGameModeBase_Statics; \
public: \
	DECLARE_CLASS(ASnake_CPlusPlusGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/Snake_CPlusPlus"), NO_API) \
	DECLARE_SERIALIZER(ASnake_CPlusPlusGameModeBase)


#define Snake_CPlusPlus_Source_Snake_CPlusPlus_Snake_CPlusPlusGameModeBase_h_15_INCLASS \
private: \
	static void StaticRegisterNativesASnake_CPlusPlusGameModeBase(); \
	friend struct Z_Construct_UClass_ASnake_CPlusPlusGameModeBase_Statics; \
public: \
	DECLARE_CLASS(ASnake_CPlusPlusGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/Snake_CPlusPlus"), NO_API) \
	DECLARE_SERIALIZER(ASnake_CPlusPlusGameModeBase)


#define Snake_CPlusPlus_Source_Snake_CPlusPlus_Snake_CPlusPlusGameModeBase_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASnake_CPlusPlusGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASnake_CPlusPlusGameModeBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASnake_CPlusPlusGameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASnake_CPlusPlusGameModeBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASnake_CPlusPlusGameModeBase(ASnake_CPlusPlusGameModeBase&&); \
	NO_API ASnake_CPlusPlusGameModeBase(const ASnake_CPlusPlusGameModeBase&); \
public:


#define Snake_CPlusPlus_Source_Snake_CPlusPlus_Snake_CPlusPlusGameModeBase_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASnake_CPlusPlusGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASnake_CPlusPlusGameModeBase(ASnake_CPlusPlusGameModeBase&&); \
	NO_API ASnake_CPlusPlusGameModeBase(const ASnake_CPlusPlusGameModeBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASnake_CPlusPlusGameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASnake_CPlusPlusGameModeBase); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASnake_CPlusPlusGameModeBase)


#define Snake_CPlusPlus_Source_Snake_CPlusPlus_Snake_CPlusPlusGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET
#define Snake_CPlusPlus_Source_Snake_CPlusPlus_Snake_CPlusPlusGameModeBase_h_12_PROLOG
#define Snake_CPlusPlus_Source_Snake_CPlusPlus_Snake_CPlusPlusGameModeBase_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Snake_CPlusPlus_Source_Snake_CPlusPlus_Snake_CPlusPlusGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	Snake_CPlusPlus_Source_Snake_CPlusPlus_Snake_CPlusPlusGameModeBase_h_15_SPARSE_DATA \
	Snake_CPlusPlus_Source_Snake_CPlusPlus_Snake_CPlusPlusGameModeBase_h_15_RPC_WRAPPERS \
	Snake_CPlusPlus_Source_Snake_CPlusPlus_Snake_CPlusPlusGameModeBase_h_15_INCLASS \
	Snake_CPlusPlus_Source_Snake_CPlusPlus_Snake_CPlusPlusGameModeBase_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Snake_CPlusPlus_Source_Snake_CPlusPlus_Snake_CPlusPlusGameModeBase_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Snake_CPlusPlus_Source_Snake_CPlusPlus_Snake_CPlusPlusGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	Snake_CPlusPlus_Source_Snake_CPlusPlus_Snake_CPlusPlusGameModeBase_h_15_SPARSE_DATA \
	Snake_CPlusPlus_Source_Snake_CPlusPlus_Snake_CPlusPlusGameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	Snake_CPlusPlus_Source_Snake_CPlusPlus_Snake_CPlusPlusGameModeBase_h_15_INCLASS_NO_PURE_DECLS \
	Snake_CPlusPlus_Source_Snake_CPlusPlus_Snake_CPlusPlusGameModeBase_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SNAKE_CPLUSPLUS_API UClass* StaticClass<class ASnake_CPlusPlusGameModeBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Snake_CPlusPlus_Source_Snake_CPlusPlus_Snake_CPlusPlusGameModeBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
